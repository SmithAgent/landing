<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use App\People;
use App\Service;
use App\Portfolio;
use DB;

class IndexController extends Controller
{
    //
    public function execute(Request $request){
        
        $pages = Page::all();
        $peoples = People::all();
        $services = Service::all();
        $portfolios = Portfolio::all();
        
        $menu = array();
        foreach($pages as $page){
            $menu[] = array('title' => $page->name,'alias' => $page->alias);
        }
        $menu[] = array('title' => 'Services','alias' => 'service');
        $menu[] = array('title' => 'Portfolio','alias' => 'Portfolio');
        $menu[] = array('title' => 'Team','alias' => 'team');
        $menu[] = array('title' => 'Contact','alias' => 'contact');
        
        //получу все уникальные значение из таблицы из поля - вернуло массив
        $categories = DB::table('portfolios')->distinct()->pluck('filter');

        return view('site.index',[
            'menu' => $menu,
            'pages' => $pages,
            'services' => $services,
            'portfolios' => $portfolios,
            'categories' => $categories,
            'peoples' => $peoples
        ]);
    }
}
