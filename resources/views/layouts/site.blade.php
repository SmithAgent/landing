<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
<title>Unique</title>

<link rel="icon" href="{{ asset('site/favicon.png') }}" type="image/png">
<link href="{{ asset('site/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('site/css/style.css') }}" rel="stylesheet" type="text/css"> 
<link href="{{ asset('site/css/font-awesome.css') }}" rel="stylesheet" type="text/css"> 
<link href="{{ asset('site/css/animate.css') }}" rel="stylesheet" type="text/css">
 
<!--[if lt IE 9]>
    <script src="{{ asset('site/js/respond-1.1.0.min.js') }}"></script>
    <script src="{{ asset('site/js/html5shiv.js') }}"></script>
    <script src="{{ asset('site/js/html5element.js') }}"></script>
<![endif]-->
 
</head>
<body>

<!--Header_section-->
<header id="header_wrapper">  
    @yield('header')
</header>
<!--Header_section--> 

<!-- content section -->
    @yield('content')
<!-- content section -->


<script type="text/javascript" src="{{ asset('site/js/jquery-1.11.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/jquery-scrolltofixed.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/jquery.nav.js') }}"></script> 
<script type="text/javascript" src="{{ asset('site/js/jquery.easing.1.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/jquery.isotope.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/wow.js') }}"></script> 
<script type="text/javascript" src="{{ asset('site/js/custom.js') }}"></script>

</body>
</html>
