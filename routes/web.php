<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function(){
    Route::match(['get','post'],'/',['uses' => 'IndexController@execute','as' => 'home']);
    Route::get('/page/{alias}', ['uses' => 'PageController@execute','as' => 'page']);

    Route::auth();
});

Route::group(['prefix' => 'admin','middleware' => 'auth'],function(){

    Route::get('/',function(){

    });

    Route::group(['prefix' => 'pages'],function(){
        Route::get('/',['uses' => 'PagesController@index','as' => 'pages']);
        Route::match(['get','post'],'/add',['uses' => 'PagesController@add','as' => 'pageAdd']);
        Route::match(['get','post','delete'],'/edit/{page}',['uses' => 'PagesController@edit','as' => 'pageEdit']); 
    });

    Route::group(['prefix' => 'portfolios'],function(){
        Route::get('/',['uses' => 'PortfoliosController@index','as' => 'portfolios']);
        Route::match(['get','post'],'/add',['uses' => 'PortfoliosController@add','as' => 'portfolioAdd']);
        Route::match(['get','post','delete'],'/edit/{portfolio}',['uses' => 'PortfoliosController@edit','as' => 'portfolioEdit']); 
    });

    Route::group(['prefix' => 'services'],function(){
        Route::get('/',['uses' => 'ServicesController@index','as' => 'services']);
        Route::match(['get','post'],'/add',['uses' => 'ServicesController@add','as' => 'serviceAdd']);
        Route::match(['get','post','delete'],'/edit/{service}',['uses' => 'ServicesController@edit','as' => 'serviceEdit']); 
    });
});
